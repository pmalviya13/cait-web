import { Component, OnInit } from '@angular/core';
import { PatientService } from '../../shared/services';

@Component({
  selector: 'app-patient-list',
  templateUrl: './patient-list.component.html',
  styleUrls: ['./patient-list.component.scss']
})
export class PatientListComponent implements OnInit {

  totalPatients
  patients
  previousLimit

  columns = [
    { field: "name", header: "Name" },
    { field: "date_of_birth", header: "Date Of Birth" },
    { field: "phone_number", header: "Phone Number" },
    { field: "aadhar_number", header: "Adhar Number" },
    { field: 'gender', header: 'Gender' },
    { field: 'created_at', header: 'Created At' },
    { field: "registered_by", header: "Registered By" }
  ];

  constructor(
    private patientService:PatientService,
  ) { }

  ngOnInit() {
    this.previousLimit=10;
  }

}
