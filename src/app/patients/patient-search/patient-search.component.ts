import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { PatientService } from '../../shared/services';

@Component({
  selector: 'app-patient-search',
  templateUrl: './patient-search.component.html',
  styleUrls: ['./patient-search.component.scss']
})
export class PatientSearchComponent implements OnInit {
  dateOfBirth='';
  phoneNumber='';
  adharNumber='';
  name='';

  totalPatients
  patients
  previousLimit
  loading = true;
  showPatientFormDialog=false;

  cols = [
    { field: "name", header: "Name" },
    { field: "date_of_birth", header: "Date Of Birth" },
    { field: "phone_number", header: "Phone Number" },
    { field: "aadhar_number", header: "Adhar Number" },
    { field: 'gender', header: 'Gender' },
    { field: 'created_at', header: 'Created At' },
    { field: "registered_by.email", header: "Registered By" }
  ];


  constructor(
    private patientService:PatientService,
    private router:Router,
  ) { }

  ngOnInit() {
    // this.name="pankaj"
    this.searchPatients()
  }

  goToPatientDetail(id){
    this.router.navigate(['/patient-detail/'+id])
  }
  searchPatients(){

    this.previousLimit=20;
    this.getPatientsList(this.previousLimit,0)
  }
  onPageChange(event,pg){
    if(this.previousLimit != event.rows){
      this.previousLimit=event.rows
      if (!pg.isFirstPage()){
        pg.changePage(0);
    }

  }
  let offset = event.rows * event.page

  this.getPatientsList(event.rows,offset)
}

maskInputClear(event){
console.log('as',this.dateOfBirth);
if(this.dateOfBirth == 'yyyy-mm-dd')
{
  this.dateOfBirth=''
  this.searchPatients()
}
}

  getPatientsList(limit,offset){

    this.patientService.getAll(limit,offset,
      this.name,this.dateOfBirth,
      this.phoneNumber,this.adharNumber).subscribe((data:any)=>{

      this.patients = data.results;
      this.totalPatients = data.count
      console.log('d',data);
      this.loading = false;
    })
  }
}
