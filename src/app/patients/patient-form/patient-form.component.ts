import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { PatientService, NotificationService } from '../../shared/services';
import { FormAbstract } from '../../shared/models';

@Component({
  selector: 'app-patient-form',
  templateUrl: './patient-form.component.html',
  styleUrls: ['./patient-form.component.scss']
})
export class PatientFormComponent extends FormAbstract implements OnInit {
  genderType

  constructor(
    private patientService:PatientService,
    private formBuilder:FormBuilder,
    private notification:NotificationService,
    private location:Location,
    private router:Router,
  ) {
    super()
  }

  ngOnInit() {
    this.genderType = [
      {label: 'Male', value: 'M'},
      {label: 'Female', value: 'F'}

  ];

    this.form = this.formBuilder.group({
      name: ['',[Validators.required]],
      address: ['',[]],
      date_of_birth: ['',[Validators.required]],
      aadhar_number: ['',[Validators.required]],
      phone_number: ['',[Validators.required]],
      gender: ['',[Validators.required]]
    })

  }

  onSubmit(){
    // if(this.form.invalid)return;
    console.log('f',this.form.value);
    this.patientService.create(this.form.value).subscribe((data:any)=>{
      console.log('d',data);
      this.notification.notify('success','Success','patient Registered Successfully')
      this.router.navigate(['/patient-detail/'+data.id])
    })
  }
  onBack(){
    this.location.back()
  }
}
