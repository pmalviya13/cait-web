import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ObservationService, NotificationService } from '../../shared/services';
import { FormAbstract } from '../../shared/models';

@Component({
  selector: 'app-observation-form',
  templateUrl: './observation-form.component.html',
  styleUrls: ['./observation-form.component.scss']
})
export class ObservationFormComponent extends FormAbstract implements OnInit {
checked
  keys = [
    {label:'sob',value:'SOB'},
    {label:'dizziness',value:'Dizziness'},
    {label:'reduced_urine',value:'Reduced Urine'},
    {label:'chest_pain',value:'Cardiac Chest Pain'},
    {label:'unconscious',value:'Drowsy/Unconscious'},
    {label:'confusion',value:'Confusion'},
    {label:'cold_extremities',value:'Cold Extremities / Skin Motting'},
    {label:'fever',value:'Fever'},
    {label:'nasal',value:'Nasal Symptoms'},
    {label:'anosmia',value:'Vomitting / Diarroea'},
    {label:'continuous_cough',value:'Anosmia'},
    {label:'body_aches',value:'Continuous Cough'},
    {label:'sore_throat',value:'Sore Throat'},
    {label:'headache',value:'Headache'}
  ]

  vomitingOptions = [
    {name: 'None', code: 'NONE'},
    {name: 'diarroea', code: 'DIARROEA'},
    {name: 'Vomiting', code: 'VOMITING'},
    {name: 'Both', code: 'BOTH'}
];

  constructor(
    private route:ActivatedRoute,
    private observationService:ObservationService,
    private notification:NotificationService,
    private formBuilder:FormBuilder,
  ) {
    super()
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      sob :['',[Validators.required]],
      dizziness :['',[Validators.required]],
      reduced_urine :['',[Validators.required]],
      chest_pain :['',[Validators.required]],
      unconscious :['',[Validators.required]],
      confusion :['',[Validators.required]],
      cold_extremities :['',[Validators.required]],
      fever :['',[Validators.required]],
      nasal :['',[Validators.required]],
      anosmia :['',[Validators.required]],
      continuous_cough :['',[Validators.required]],
      body_aches :['',[Validators.required]],
      sore_throat :['',[Validators.required]],
      headache :['',[Validators.required]],

      heart_rate :['',[Validators.required]],
      vomiting :['',[Validators.required]],
      body_temperature :['',[Validators.required]],
      rt_pcr_test :['',[Validators.required]],
      existing_illness :['',[Validators.required]],
      o2_saturation :['',[Validators.required]],
      respiratory_rate :['',[Validators.required]]

    })
  }

}
