import { ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { PatientService, UserService } from '../../shared/services';
import { FormAbstract } from '../../shared/models';

@Component({
  selector: 'app-patient-detail',
  templateUrl: './patient-detail.component.html',
  styleUrls: ['./patient-detail.component.scss']
})
export class PatientDetailComponent extends FormAbstract implements OnInit {

  patient;
  doctor

  editProfile:boolean;
  genderType
  constructor(
    private patientService:PatientService,
    private userService:UserService,
    private route:ActivatedRoute,

  ) {
    super()
   }

  ngOnInit() {

    this.getPatient()

  }
  patchValues(){
    this.form.patchValue({
      name:this.patient.name,
      address:this.patient.address,
      date_of_birth:this.patient.date_of_birth,
      phone_number:this.patient.phone_number,
      aadhar_number:this.patient.aadhar_number,
      gender:this.patient.gender
    })
  }

  getPatient(){
    this.patientService.getById(100).subscribe((data:any)=>{
      this.patient=data;
      console.log('patient',this.patient);
      this.getDoctor(data.registered_by.id)
    })
  }

  getDoctor(id){
    this.userService.getById(id).subscribe(data=>{
      this.doctor=data
    })
  }
}
