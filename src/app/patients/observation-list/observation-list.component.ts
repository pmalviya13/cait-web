import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';
import { ObservationService } from '../../shared/services';

@Component({
  selector: 'app-observation-list',
  templateUrl: './observation-list.component.html',
  styleUrls: ['./observation-list.component.scss']
})
export class ObservationListComponent implements OnInit {
  @Input() patientId;

  observations;
  keys = [
    {label:'sob',value:'SOB'},
    {label:'dizziness',value:'Dizziness'},
    {label:'reduced_urine',value:'Reduced Urine'},
    {label:'chest_pain',value:'Cardiac Chest Pain'},
    {label:'unconscious',value:'Drowsy/Unconscious'},
    {label:'confusion',value:'Confusion'},
    {label:'cold_extremities',value:'Cold Extremities / Skin Motting'},
    {label:'fever',value:'Fever'},
    {label:'nasal',value:'Nasal Symptoms'},
    {label:'anosmia',value:'Vomitting / Diarroea'},
    {label:'continuous_cough',value:'Anosmia'},
    {label:'body_aches',value:'Continuous Cough'},
    {label:'sore_throat',value:'Sore Throat'},
    {label:'headache',value:'Headache'}
  ]
  constructor(
    private observationService:ObservationService,
    private route:ActivatedRoute,
  ) { }

  ngOnInit() {
    this.getAllObservations()
  }


  getAllObservations(){
    this.observationService.getAll(this.patientId).subscribe(data =>{
      this.observations=data
    })
  }
}
