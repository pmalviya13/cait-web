import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AccordionModule} from 'primeng/accordion';
import {ButtonModule} from 'primeng/button';
import {ToastModule} from 'primeng/toast';
import {CardModule} from 'primeng/card';
import {PanelModule} from 'primeng/panel';
import { DynamicDialogModule, DialogService } from 'primeng/dynamicdialog';
import {InputTextModule} from 'primeng/inputtext';
import {StepsModule} from 'primeng/steps';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {MessagesModule} from 'primeng/messages';
import {MessageModule} from 'primeng/message';
import {TabViewModule} from 'primeng/tabview';
import {ScrollPanelModule} from 'primeng/scrollpanel';
import {InplaceModule} from 'primeng/inplace';
import {DialogModule} from 'primeng/dialog';
import {MessageService} from 'primeng/api';
import {OverlayPanelModule} from 'primeng/overlaypanel';
import {DataViewModule} from 'primeng/dataview';
import {TooltipModule} from 'primeng/tooltip';
import {ChipsModule} from 'primeng/chips';
import {DragDropModule} from 'primeng/dragdrop';
import {AutoCompleteModule} from 'primeng/autocomplete';
import {ProgressSpinnerModule} from 'primeng/progressspinner';
import {FileUploadModule} from 'primeng/fileupload';
import {GalleriaModule} from 'primeng/galleria';
import {VirtualScrollerModule} from 'primeng/virtualscroller';
import {DropdownModule} from 'primeng/dropdown';
import {SidebarModule} from 'primeng/sidebar';
import {PickListModule} from 'primeng/picklist';
import {ChartModule} from 'primeng/chart';
import {ListboxModule} from 'primeng/listbox';
import {ToggleButtonModule} from 'primeng/togglebutton';
import {MultiSelectModule} from 'primeng/multiselect';
import {CarouselModule} from 'primeng/carousel';
import {CodeHighlighterModule} from 'primeng/codehighlighter';
import {FieldsetModule} from 'primeng/fieldset';
import {SelectButtonModule} from 'primeng/selectbutton';
import {CheckboxModule} from 'primeng/checkbox';
import {TableModule} from 'primeng/table';
import {ProgressBarModule} from 'primeng/progressbar';
import {InputSwitchModule} from 'primeng/inputswitch';
import {TreeTableModule} from 'primeng/treetable';
import {RadioButtonModule} from 'primeng/radiobutton';
import {CalendarModule} from 'primeng/calendar';
import {SliderModule} from 'primeng/slider';
import {PaginatorModule} from 'primeng/paginator';
import {MenubarModule} from 'primeng/menubar';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {InputMaskModule} from 'primeng/inputmask';
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AccordionModule,
    ButtonModule,
    ToastModule,
    CardModule,
    PanelModule,
    DynamicDialogModule,
    InputTextModule,
    StepsModule,
    ConfirmDialogModule,
    MessagesModule,
    MessageModule,
    TabViewModule,
    ScrollPanelModule,
    InplaceModule,
    DialogModule,
    OverlayPanelModule,
    DataViewModule,
    TooltipModule,
    ChipsModule,
    DragDropModule,
    AutoCompleteModule,
    PickListModule,
    FileUploadModule,
    GalleriaModule,
    ChartModule,
    ListboxModule,
    VirtualScrollerModule,
    DropdownModule,
    SidebarModule,
    ToggleButtonModule,
    MultiSelectModule,
    VirtualScrollerModule,
    CarouselModule,CodeHighlighterModule,
    ProgressSpinnerModule,
    FieldsetModule,
    SelectButtonModule,
    CheckboxModule,
    ProgressBarModule,
    TableModule,
    TreeTableModule,
    InputSwitchModule,
    RadioButtonModule,
    SliderModule,
    CalendarModule,
    PaginatorModule,
    MenubarModule,
    InputTextareaModule,
    InputMaskModule,


  ],
  exports: [
    AccordionModule,
    ButtonModule,
    ToastModule,
    CardModule,
    PanelModule,
    DynamicDialogModule,
    InputTextModule,
    StepsModule,
    ConfirmDialogModule,
    MessagesModule,
    MessageModule,
    TabViewModule,
    ScrollPanelModule,
    InplaceModule,
    DialogModule,
    OverlayPanelModule,
    DataViewModule,
    TooltipModule,
    ChipsModule,
    DragDropModule,
    AutoCompleteModule,
    PickListModule,
    FileUploadModule,
    GalleriaModule,
    ChartModule,
    ListboxModule,
    ToggleButtonModule,
    MultiSelectModule,
    VirtualScrollerModule,
    CarouselModule,
    CodeHighlighterModule,
    GalleriaModule,
    ListboxModule,
    VirtualScrollerModule,
    DropdownModule,
    SidebarModule,
    ProgressSpinnerModule,
    FieldsetModule,
    SelectButtonModule,
    CheckboxModule,
    ProgressBarModule,
    TableModule,
    TreeTableModule,
    InputSwitchModule,
    RadioButtonModule,
    SliderModule,
    CalendarModule,
    PaginatorModule,
    MenubarModule,
    InputTextareaModule,
    InputMaskModule,


  ],
  providers: [
    { provide : MessageService
    },
    {provide: DialogService}
  ]
})
export class AppPrimengModule { }
