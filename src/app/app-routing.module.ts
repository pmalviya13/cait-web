import { PatientFormComponent } from './patients/patient-form/patient-form.component';
import { PatientDetailComponent } from './patients/patient-detail/patient-detail.component';
import { VerifyEmailComponent } from './accounts/verify-email/verify-email.component';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './accounts/dashboard/dashboard.component';
import { RegisterComponent } from './accounts/register/register.component';
import { LoginComponent } from './accounts/login/login.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './shared/gaurds';


const routes: Routes = [
  {path:'',component:HomeComponent},
  {path:'login',component:LoginComponent},
  {path:'register',component:RegisterComponent},
  {path:'dashboard',component:DashboardComponent, canActivate: [AuthGuard]},
  {path:'verify-email/:token',component:VerifyEmailComponent},
  {path:'patient-detail/:id',component:PatientDetailComponent},
  {path:'patient',component:PatientFormComponent, canActivate: [AuthGuard]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
