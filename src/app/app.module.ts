import { NotificationService } from './shared/services/notification.service';
import { AppPrimengModule } from './app-primeng/app-primeng.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TopNavComponent } from './shared/components/top-nav/top-nav.component';
import { LoginComponent } from './accounts/login/login.component';
import { RegisterComponent } from './accounts/register/register.component';
import { DashboardComponent } from './accounts/dashboard/dashboard.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HomeComponent } from './home/home.component';
import { NotificationComponent } from './shared/components/notification/notification.component';
import { TokenInterceptor } from './shared/interceptors';
import { VerifyEmailComponent } from './accounts/verify-email/verify-email.component';
import { PatientListComponent } from './patients/patient-list/patient-list.component';
import { PatientDetailComponent } from './patients/patient-detail/patient-detail.component';
import { PatientFormComponent } from './patients/patient-form/patient-form.component';
import { PatientSearchComponent } from './patients/patient-search/patient-search.component';
import { ObservationListComponent } from './patients/observation-list/observation-list.component';
import { YesNoPipe } from './shared/pipes/yes-no.pipe';
import { PosNegPipe } from './shared/pipes/pos-neg.pipe';
import { ObservationFormComponent } from './patients/observation-form/observation-form.component';

@NgModule({
  declarations: [
    AppComponent,
    TopNavComponent,
    LoginComponent,
    RegisterComponent,
    DashboardComponent,
    HomeComponent,
    NotificationComponent,
    VerifyEmailComponent,
    PatientListComponent,
    PatientDetailComponent,
    PatientFormComponent,
    PatientSearchComponent,
    ObservationListComponent,
    YesNoPipe,
    PosNegPipe,
    ObservationFormComponent,
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    AppRoutingModule,
    AppPrimengModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    FlexLayoutModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    NotificationService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
