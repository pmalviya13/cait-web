import { Component, OnInit } from '@angular/core';
import { AuthService } from './shared/services';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Corona fighters';
  isLoggedIn
  constructor(private authService:AuthService){

  }
  ngOnInit(){
    this.getloginStatus()
  }

  getloginStatus(){
    this.authService.isLoggedIn().subscribe(data =>{
      console.log('loggd in',data);

      this.isLoggedIn = data
    })
  }
}
