import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { UserService } from '../../shared/services';

@Component({
  selector: 'app-verify-email',
  templateUrl: './verify-email.component.html',
  styleUrls: ['./verify-email.component.scss']
})
export class VerifyEmailComponent implements OnInit {
token;
invalidToken:boolean;
loading=true
message
  constructor(
    private userService:UserService,
    private route:ActivatedRoute,
    private router:Router,
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.token = params.get('token');
      setTimeout(()=>{
        this.verifyEmail()
      },2000)
    });
  }

  verifyEmail(){
    this.userService.verifyToken(this.token).subscribe((data:any)=>{
      this.message = "Your email is successfully verified. redirecting to login."
      setTimeout(() => {
        this.router.navigate(['login'])
      }, 2000);
    }, error =>{
      this.invalidToken=true;
      this.message = "Invalid token, Please resend email to verify account"
      setTimeout(() => {
        this.router.navigate([''])
      }, 2000);
    })
  }
}
