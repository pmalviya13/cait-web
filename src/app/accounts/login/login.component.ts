
import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormAbstract } from '../../shared/models';
import { AuthService } from '../../shared/services';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent extends FormAbstract implements OnInit  {
  loading = false;
  returnUrl: string;
  hide = true;
  errorMessage: string;
  formValid = true;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private authService: AuthService,
    ) {
      super()
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
  });
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || 'dashboard';
  }

  onSubmit() {
    if (this.form.invalid)return;
    this.errorMessage=''
    this.authService.login(this.form.value)
    .subscribe(success => {
        this.authService.UserLoggedIn(success);
        this.router.navigate([this.returnUrl]);
    }, error => {
      this.errorMessage = 'Invalid Username or Password.';
    });
  }

}
