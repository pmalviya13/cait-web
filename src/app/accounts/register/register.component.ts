import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { FormAbstract } from '../../shared/models';
import { AuthService, UserService, NotificationService } from '../../shared/services';
import { UserFormValidator } from '../../shared/validators';
import { Message } from 'primeng/api/message';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent extends FormAbstract implements OnInit {

  loading = false;
  returnUrl: string;
  hide = true;
  errorMessage: string;
  formValid = true;

  alreadRegistered=false;
  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private authService: AuthService,
    private userService:UserService,
    private notification:NotificationService
    ) {
      super()
  }

  ngOnInit() {
    this.form  = this.formBuilder.group({
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      email: ['', [Validators.required,Validators.email]],
      password: ['', [Validators.required,Validators.minLength(8),UserFormValidator.passwordValidator]], //Validators.minLength(6),UserFormValidator.passwordValidator
      // confirm_password: ['', [Validators.required] ]
  });
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || 'dashboard';
  }

  onSubmit() {
    if (this.form.invalid)return;
    this.errorMessage=''
    this.userService.create(this.form.value).subscribe((data)=>{
      this.router.navigate(['/'])
    }, error =>{
      console.log('eror',error);

      if(error.error.errors.email[0] == "user with this email address already exists."){
        this.alreadRegistered=true
      }
    })
  }


  resendEmail(){
    let data = {
      email:this.form.value.email
    }
    this.userService.resendEmail(data).subscribe((data:any)=>{
      this.notification.notify("success","Succesfully sent",data.email)
    })
  }

}
