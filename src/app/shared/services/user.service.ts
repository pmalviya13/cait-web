import { config } from './../config/config';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http:HttpClient) { }
  create(data){
    return this.http.post(config.apiUrl+'/accounts/',data)
  }
  getById(id){
    return this.http.get(config.apiUrl+'/accounts/'+id)
  }

  resendEmail(data){
    return this.http.post(config.apiUrl+'/accounts/resend-email/',data)
  }
  verifyToken(token){
    return this.http.get(config.apiUrl+'/accounts/verify-token/'+token)
  }
}
