import { config } from './../config/config';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ObservationService {

  constructor(private http:HttpClient) { }

  getAll(id){
    return this.http.get(config.apiUrl+'/patients/'+id+'/observations')
  }
  create(id,data){
    return this.http.post(config.apiUrl+'/patients/'+id+'/observations',data)
  }
  update(id,data){
    return this.http.put(config.apiUrl+'/patients/'+id+'/observations',data)
  }
}
