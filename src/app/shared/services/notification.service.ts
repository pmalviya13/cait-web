import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';


type Severities = 'success' | 'info' | 'warn' | 'error';


@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor() { }
  notificationChange: Subject<Object> = new Subject<Object>();

  notify(severity: Severities, summary: string, detail: string) {
    console.log('service called');

    this.notificationChange.next({ severity, summary, detail });
  }

}
