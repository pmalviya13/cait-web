import { config } from './../config/config';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PatientService {

  constructor(
    private http:HttpClient,
  ) { }

  getAll(limit,offset,name,dob,phoneNumber,adharNumber){
    let url = `${config.apiUrl}/patients?limit=${limit}&offset=${offset}&phone_number=${phoneNumber}&aadhar_number=${adharNumber}&date_of_birth=${dob}&name=${name}`
    console.log('url',url);

    return this.http.get(url)
  }
  getById(id){
    return this.http.get(`${config.apiUrl}/patients/${id}`)
  }
  updatePatient(id,data){
    return this.http.put(`${config.apiUrl}/patients/${id}`,data)
  }
  create(data){
    return this.http.post(config.apiUrl+'/patients/',data)
  }
}
