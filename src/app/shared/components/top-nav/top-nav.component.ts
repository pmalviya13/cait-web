import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import {MenuItem} from 'primeng/api';
import { Subscription } from 'rxjs';
import { AuthService } from '../../services';

@Component({
  selector: 'app-top-nav',
  templateUrl: './top-nav.component.html',
  styleUrls: ['./top-nav.component.scss']
})
export class TopNavComponent implements OnInit {
  loggedIn: boolean;
  subscription: Subscription;

  ngOnInit() {

}
  constructor(private authService: AuthService,
    private router:Router) {
    this.subscription = this.authService.isUserLoggedIn.subscribe(res => this.isLoggedIn(res) );
    if (localStorage.getItem('JWT_TOKEN')) {
      this.loggedIn = true;
    } else {
      this.loggedIn = false;
    }
    console.log('is logged in',this.loggedIn);

  }
  isLoggedIn(res) {
    if (res) {
      this.loggedIn = true;
    } else {
      this.loggedIn = false;
    }
  }

  logout(){
    this.authService.logout()
    this.router.navigate(['/'])
  }
}
