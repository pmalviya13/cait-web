import { Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Message } from 'primeng/api/message';
import { MessageService } from "primeng/api";
import { NotificationService } from '../../services';
@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss'],
  providers: [MessageService]
})
export class NotificationComponent implements OnInit, OnDestroy  {


  msgs: Message[] = [];
  subscription: Subscription;

  constructor(private notificationService: NotificationService,
    private messageService:MessageService) { }

  ngOnInit() {
    console.log('init');

    this.subscribeToNotifications();
  }
  subscribeToNotifications() {
    this.subscription = this.notificationService.notificationChange
    .subscribe(notification => {
      this.msgs.length = 0;
      this.msgs.push(notification);
      this.messageService.add(notification)
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
