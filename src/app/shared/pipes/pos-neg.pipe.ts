import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'posNeg'
})
export class PosNegPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    if(value == 'POS')return 'Positive'
    if(value == 'NEG')return 'Negative'
    return value
  }

}
