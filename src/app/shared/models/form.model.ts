import { FormGroup } from "@angular/forms";

export abstract class FormAbstract  {
    form: FormGroup;
    submitted: boolean = false; // a flag to be used in template to indicate whether the user tried to submit the form

    public hasError(controlName: string, errorName: string) {
      return this.form.controls[controlName].hasError(errorName)
    }

    resetForm() {
      this.form.reset();
    }
    get f() { return this.form.controls; }
    onSubmit() {

      this.submitted = true;
      if (this.form.invalid) {

        return;
      }

    }

  }
